# Blood Donation

[![build status](https://git.mob-dev.fr/Schoumi/BloodDonation/badges/master/build.svg)](https://git.mob-dev.fr/Schoumi/BloodDonation/commits/master)

Blood Donation is an Android application.
Blood Donation is an independant initiative. It allows user to manage donation to the French Establishment of Blood (EFS) in an easy way.

It allows you to:
  * View donation center around you or a preregistered place
  * Be notified of next donation campaign
  * Add a donation campaign date to your agenda and to ask for direction to the place
  * Track your previous donation
  * Call Post-Donation Call Center near you in case you had to

Download it on the playstore: https://play.google.com/store/apps/details?id=fr.mobdev.blooddonation

<br/>
<noscript><a href="https://liberapay.com/Schoumi/donate"><img src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
