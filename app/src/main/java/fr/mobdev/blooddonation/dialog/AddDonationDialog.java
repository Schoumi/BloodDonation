/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.dialog;

import java.util.Calendar;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.NewDonationListener;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.enums.DonationType;
import fr.mobdev.blooddonation.objects.Donation;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

@SuppressLint("InflateParams")
public class AddDonationDialog extends DialogFragment {

	private int siteId;
	private NewDonationListener listener;

	public AddDonationDialog()
	{
		siteId = -1;
	}


	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View view = inflater.inflate(R.layout.add_donation_dialog, null);
		DatePicker date_picker =  view.findViewById(R.id.add_date_picker);
		date_picker.setMaxDate(Calendar.getInstance().getTimeInMillis());
		builder
		.setView(view)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				Spinner donationType = view.findViewById(R.id.add_donnation_type_spinner);
				EditText name_texte = view.findViewById(R.id.add_city_text);
				DatePicker date_picker = view.findViewById(R.id.add_date_picker);

				siteId = getArguments().getInt("siteId", -1 );

				DonationType type;
				if(donationType.getSelectedItemPosition() == 0)
					type = DonationType.TOTAL_BLOOD;
				else if(donationType.getSelectedItemPosition() == 1)
					type = DonationType.PLASMA;
				else
					type = DonationType.PLATELET;

				String city_name = null;
				if(siteId == -1)
					city_name = name_texte.getText().toString();
				//else
					//use city from donation site

				Calendar date = Calendar.getInstance();// = date_picker.get
				date.set(date_picker.getYear(), date_picker.getMonth(), date_picker.getDayOfMonth(), 12, 0);

				Donation donation = new Donation(type, date, siteId, -1, city_name);
				Database.getInstance(getActivity().getApplicationContext()).addDonation(donation);
				listener.needUpdate();
			}
		})
		.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		return builder.create();
	}


	public void setListener(NewDonationListener listener) {
		this.listener = listener;
	}
}
