/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.dialog;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.RegisterListener;
import fr.mobdev.blooddonation.enums.DonationType;
import fr.mobdev.blooddonation.objects.BloodSite;
import fr.mobdev.blooddonation.objects.Donation;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class ConfirmDialog extends DialogFragment {

	private long siteId;
	private RegisterListener registerListener;

	public ConfirmDialog()
	{
		siteId = -1;
	}


	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		siteId = getArguments().getLong("siteId", -1 );
		if (siteId == -1)
			return null;
		
		List<BloodSite> sites = Database.getInstance(getActivity()).getBloodSites(siteId);
		if(sites.size() != 1)
			return null;
		final BloodSite site = sites.get(0);
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
		
		String message = getString(R.string.confirm);
		message +=" ";
		message += df.format(site.getDate().getTime());
		message +=" ";
		message += getString(R.string.confirm2);
		message +=" ";
		message += site.getCityName();
		message +=".\n";
		message += getString(R.string.confirm3);
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.confirm_dialog, null);
		
		TextView messageView = view.findViewById(R.id.confirm_message);
		final Spinner spinner = view.findViewById(R.id.confirm_spinner);
		
		messageView.setText(message);
		
		builder
		.setView(view)
		.setTitle(getString(R.string.confirm_title))
		.setPositiveButton(R.string.confirm_donation, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				
				DonationType type;
				switch(spinner.getSelectedItemPosition())
				{
				case 1:
					type = DonationType.PLASMA;
					break;
				case 2:
					type = DonationType.PLATELET;
					break;
				case 0:
				default:
					type = DonationType.TOTAL_BLOOD;
					break;
				}
				Calendar date = site.getDate();
				Donation donation = new Donation(type,date,siteId,-1,site.getCityName());
				Database.getInstance(getActivity()).addDonation(donation);
				Database.getInstance(getActivity()).scheduleDonation(siteId, false);
				if(registerListener != null)
					registerListener.processNewRegistration();
			}
		})
		.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Database.getInstance(getActivity()).scheduleDonation(siteId, false);
				if(registerListener != null)
					registerListener.processNewRegistration();
				dialog.cancel();
			}
		});
		return builder.create();
	}

	public void setRegisterListener(RegisterListener listener) {
		registerListener = listener;
	}
}
