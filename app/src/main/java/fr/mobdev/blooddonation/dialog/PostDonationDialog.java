/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.dialog;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import fr.mobdev.blooddonation.R;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class PostDonationDialog extends DialogFragment {

	private boolean changeMode;
	private int depId;
	private View view;
	private Button neutralButton;

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		neutralButton = null;

		LayoutInflater inflater = getActivity().getLayoutInflater();
		view = inflater.inflate(R.layout.post_donation_dialog, null);
		if(getArguments() != null)
			depId = getArguments().getInt("depId",-1);
		else
			depId = -1;
		final Spinner depList = view.findViewById(R.id.dep_spinner);
		ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.dep_array));
		depList.setAdapter(adapter);
		depList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View viewSelected, int position, long id) {
				if(position >= 95)
				{
					if(position+1 == 96)
						depId = 971;
					else if(position+1 == 97)
						depId = 972;
					else if(position+1 == 98)
						depId = 973;
					else if(position+1 == 99)
						depId = 974;
				}
				else
				{
					depId = position+1;
				}
				TextView depView = view.findViewById(R.id.dep_name);
				if(position > 0){
					depView.setText(getResources().getStringArray(R.array.dep_array)[position]);
				}
				changeNumbers();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing
			}
		});
		if(depId > 1)
			depList.setSelection(depId-1);
		TextView depView = view.findViewById(R.id.dep_name);

		if(depId != -1)
		{
			int position = depId;
			if(depId == 971)
				position = 96;
			else if(depId == 972)
				position = 97;
			else if(depId == 973)
				position = 98;
			else if(depId == 974)
				position = 99;
			depView.setText(getResources().getStringArray(R.array.dep_array)[position-1]);
		}
		else
		{
			depView.setText(getResources().getStringArray(R.array.dep_array)[0]);			
		}

		changeSelectionMode(false);



		changeNumbers();

		//get list of number for depId
		//setup list if number list contains more 1
		//
		builder
		.setTitle(getString(R.string.post_numbers))
		.setView(view)
		.setNeutralButton(R.string.change_region,null)
		.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		final AlertDialog dialog = builder.create();

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialogI) {
				neutralButton = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
				neutralButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						changeSelectionMode(true);
					}
				});
				changeSelectionMode(false);	
			}
		});
		return dialog;
	}

	private void changeNumbers()
	{
		TextView numberView =  view.findViewById(R.id.number_to_call);
		Spinner numberSpinner =  view.findViewById(R.id.number_spinner);
		HashMap<String, String> numberMap = getNumbersFor(depId);

		ArrayList<String> numberList = new ArrayList<>();
		for(String key : numberMap.keySet())
		{
			numberList.add(key+" - "+numberMap.get(key));
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, numberList);
		numberSpinner.setAdapter(adapter);

		if(numberMap.size() > 1)
		{	
			numberSpinner.setVisibility(View.VISIBLE);
			numberView.setVisibility(View.GONE);
		}
		else if(numberMap.size() == 1)
		{
			numberView.setText(numberMap.get(""));
			numberView.setVisibility(View.VISIBLE);
			numberSpinner.setVisibility(View.GONE);
		}
		else
		{
			numberView.setVisibility(View.VISIBLE);
			numberSpinner.setVisibility(View.GONE);
			numberView.setText(getString(R.string.no_phone_available));
			changeSelectionMode(false);
		}
	}

	private void changeSelectionMode(boolean buttonPressed)
	{
		Spinner depList = view.findViewById(R.id.dep_spinner);
		TextView depView = view.findViewById(R.id.dep_name);
		HashMap<String,String> numberMap = getNumbersFor(depId);
		if(buttonPressed)
			changeMode = !changeMode;

		if(depId == -1 || changeMode || numberMap.size() == 0)
		{
			depList.setVisibility(View.VISIBLE);
			depView.setVisibility(View.GONE);
			if(neutralButton != null)
				neutralButton.setText(getString(R.string.fix_region));
		}
		else
		{
			depList.setVisibility(View.GONE);
			depView.setVisibility(View.VISIBLE);
			if(neutralButton != null)
				neutralButton.setText(getString(R.string.change_region));
		}
	}

	private HashMap<String,String> getNumbersFor(int depId){
		HashMap<String,String> numberMap = new HashMap<>();
		switch(depId)
		{
		case 1:
			numberMap.put("","0472117534");
			break;
		case 2:
			numberMap.put("","0820802222");
			break;
		case 3:
			numberMap.put("","0473152161");
			break;
		case 4:
			numberMap.put("","0810810109");
			break;
		case 5:
			numberMap.put("","0810810109");
			break;
		case 6:
			numberMap.put("","0810810109");
			break;
		case 7:
			numberMap.put("","0472117534");
			break;
		case 8:
			numberMap.put("","0820802222");
			break;
		case 9:
			numberMap.put("","0800972100");
			break;
		case 10:
			numberMap.put("","0810207100");
			break;
		case 11:
			numberMap.put("","0800972100");
			break;
		case 12:
			numberMap.put("","0800972100");
			break;
		case 13:
			numberMap.put("","0810810109");
			break;
		case 14:
			numberMap.put("","0231535353");
			break;
		case 15:
			numberMap.put("","0473152161");
			break;
		case 16:
			numberMap.put("","0800732100");
			break;
		case 17:
			numberMap.put("","0800732100");
			break;
		case 18:
			numberMap.put("","0800732100");
			break;
		case 19:
			numberMap.put("","0800744100");
			break;
		case 20:
			numberMap.put("","0810810109");
			break;
		case 21:
			numberMap.put("","0380706010");
			break;
		case 22:
			break;
		case 23:
			numberMap.put("","0800744100");
			break;
		case 24:
			numberMap.put("","0800744100");
			break;
		case 25:
			numberMap.put("","0381615615");
			break;
		case 26:
			numberMap.put("","0472117534");
			break;
		case 27:
			numberMap.put("","0232330167");
			break;
		case 28:
			numberMap.put("","0800732100");
			break;
		case 29:
			numberMap.put("Avant 17h00","0298445077");
			numberMap.put("Après 17h00","0298347817");
			break;
		case 30:
			numberMap.put("","0800972100");
			break;
		case 31:
			numberMap.put("","0800972100");
			break;
		case 32:
			numberMap.put("","0800972100");
			break;
		case 33:
			numberMap.put("","0800744100");
			break;
		case 34:
			numberMap.put("","0800972100");
			break;
		case 35:
			numberMap.put("","0299544222");
			break;
		case 36:
			numberMap.put("","0800732100");
			break;
		case 37:
			numberMap.put("","0800732100");
			break;
		case 38:
			numberMap.put("","0472117534");
			break;
		case 39:
			numberMap.put("","0381615615");
			break;
		case 40:
			numberMap.put("","0800744100");
			break;
		case 41:
			numberMap.put("","0800732100");
			break;
		case 42:
			numberMap.put("","0477928558");
			break;
		case 43:
			numberMap.put("","0477928558");
			break;
		case 44:
			numberMap.put("Nantes","0240123362");
			numberMap.put("Saint-Nazaire","0240002000");
			break;
		case 45:
			numberMap.put("","0800732100");
			break;
		case 46:
			numberMap.put("","0800972100");
			break;
		case 47:
			numberMap.put("","0800744100");
			break;
		case 48:
			numberMap.put("","0800972100");
			break;
		case 49:
			numberMap.put("","0241724444");
			break;
		case 50:
			numberMap.put("Saint-Lô","0233063300");
			numberMap.put("Cherbourg","0233207632");
			break;
		case 51:
			numberMap.put("","0820802222");
			break;
		case 52:
			numberMap.put("","0810207100");
			break;
		case 53:
			numberMap.put("","0243669000");
			break;
		case 54:
			numberMap.put("","0810207100");
			break;
		case 55:
			numberMap.put("","0810207100");
			break;
		case 56:
			numberMap.put("Vannes","0297675300");
			numberMap.put("Lorient","0297069293");
			break;
		case 57:
			numberMap.put("","0810207100");
			break;
		case 58:
			numberMap.put("","0386618200");
			break;
		case 59:
			numberMap.put("","0820802222");
			break;
		case 60:
			break;
		case 61:
			numberMap.put("","0233264818");
			break;
		case 62:
			numberMap.put("","0820802222");
			break;
		case 63:
			numberMap.put("","0473152161");
			break;
		case 64:
			numberMap.put("","0800744100");
			break;
		case 65:
			numberMap.put("","0800972100");
			break;
		case 66:
			numberMap.put("","0800972100");
			break;
		case 67:
			numberMap.put("","0388212525");
			break;
		case 68:
			numberMap.put("","0388212525");
			break;
		case 69:
			numberMap.put("","0472117534");
			break;
		case 70:
			numberMap.put("","0381615615");
			break;
		case 71:
			numberMap.put("Mâcon","0385342200");
			numberMap.put("Chalon sur Saône","0385427490");
			break;
		case 72:
			numberMap.put("","0243399494");
			break;
		case 73:
			numberMap.put("","0472117534");
			break;
		case 74:
			numberMap.put("","0472117534");
			break;
		case 75:
			numberMap.put("","0153912398");
			break;
		case 76:
			numberMap.put("Dieppe","0232147615");
			numberMap.put("Rouen - Bois Guillaume","0235605050");
			numberMap.put("Rouen - Saint Sever","0232109780");
			numberMap.put("Le Hâvre","0235211933");
			break;
		case 77:
			numberMap.put("","0153912398");
			break;
		case 78:
			numberMap.put("","0153912398");
			break;
		case 79:
			numberMap.put("","0800732100");
			break;
		case 80:
			numberMap.put("","0820802222");
			break;
		case 81:
			numberMap.put("","0800972100");
			break;
		case 82:
			numberMap.put("","0800972100");
			break;
		case 83:
			numberMap.put("","0810810109");
			break;
		case 84:
			numberMap.put("","0810810109");
			break;
		case 85:
			numberMap.put("","0251446361");
			break;
		case 86:
			numberMap.put("","0800732100");
			break;
		case 87:
			numberMap.put("","0800744100");
			break;
		case 88:
			numberMap.put("","0810207100");
			break;
		case 89:
			numberMap.put("Auxerre","0386420370");
			numberMap.put("Sens","0386646163");
			break;
		case 90:
			numberMap.put("","0384584990");
			break;
		case 91:
			numberMap.put("","0153912398");
			break;
		case 92:
			numberMap.put("","0153912398");
			break;
		case 93:
			numberMap.put("","0153912398");
			break;
		case 94:
			numberMap.put("","0153912398");
			break;
		case 95:
			numberMap.put("","0153912398");
			break;
		case 971:
			numberMap.put("","0590471820");
			break;
		case 972:
			numberMap.put("","0596757900");
			break;
		case 973:
			numberMap.put("","0262905390");
			break;
		case 974:
			numberMap.put("","0262359064");
			break;
		}
		return numberMap;
	}

}
