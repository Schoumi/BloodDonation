/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.dialog;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.enums.SiteType;
import fr.mobdev.blooddonation.objects.BloodSite;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class InformationDialog extends DialogFragment {
	
	private long siteId;
	
	public InformationDialog()
	{
		siteId = -1;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		siteId = getArguments().getLong("siteId", -1 );
		if (siteId == -1)
		{
			Log.e("infoDialog", "siteId in args");
			return null;
		}
		
		List<BloodSite> sites = Database.getInstance(getActivity()).getBloodSites(siteId);
		if(sites.size() != 1)
		{
			Log.e("infoDialog", "siteId in db");
			return null;
		}
		final BloodSite site = sites.get(0);
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
		String title = site.getCityName();
		if(site.getDate()!=null)
		{
			title += " - ";
			title += df.format(site.getDate().getTime());
		}
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.information_dialog, null);
		
		TextView detailsView = view.findViewById(R.id.details);
		TextView addressView = view.findViewById(R.id.address);
		
		//parse details to find begin and end time
		detailsView.setText(site.getDetails());
		addressView.setText(site.getAddress());
		
		

		
		builder
		.setView(view)
		.setTitle(title)
		.setPositiveButton(R.string.go_iti, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+site.getLoc().latitude+","+site.getLoc().longitude));
			        startActivity(intent);				
			}
		})
		.setNeutralButton(R.string.add_agenda, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_INSERT);
				intent.setType("vnd.android.cursor.item/event");
				if(site.getType() != SiteType.efs)
				{
					String begin = site.getDetails();
					begin = begin.substring(0,begin.indexOf("à"));
					begin = begin.substring(begin.indexOf("h")-2, begin.indexOf("h")+3);
					final int beginHour = Integer.valueOf(begin.substring(0,2)) * 1000 * 60 *60 + Integer.valueOf(begin.substring(3,5))*1000*60;
					String end = site.getDetails();
					end = end.substring(end.lastIndexOf("à"));
					end = end.substring(end.indexOf("h")-2, end.indexOf("h")+3);
					final int endHour = Integer.valueOf(end.substring(0,2)) * 1000 * 60 *60 + Integer.valueOf(end.substring(3,5))*1000*60;
					intent.putExtra("beginTime", site.getDate().getTimeInMillis()+beginHour);
					intent.putExtra("endTime", site.getDate().getTimeInMillis()+endHour);
					Database.getInstance(getActivity()).scheduleDonation(siteId, true);
				}
				
				intent.putExtra("allDay", false);
				intent.putExtra("title", "Don du Sang");
                intent.putExtra("description",site.getSiteName());
				intent.putExtra("eventLocation", ""+site.getLoc().latitude+","+site.getLoc().longitude);
				startActivity(intent);
			}
		})
		.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		return builder.create();
	}
}
