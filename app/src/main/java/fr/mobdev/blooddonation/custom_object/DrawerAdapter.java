/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.custom_object;

import java.util.List;

import androidx.annotation.NonNull;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.enums.DrawerItemType;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {

	private List<DrawerItem> items;
	private LayoutInflater mInflater;
	
	public DrawerAdapter(Context context, int resource, int textViewResourceId,	List<DrawerItem> objects) {
		super(context, resource, textViewResourceId, objects);
		mInflater = LayoutInflater.from(context);
		items = objects;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		ViewHolder holder;
		DrawerItem item = items.get(position);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.drawer_item, parent, false);
		}
		holder = new ViewHolder();
		holder.pos = convertView.findViewById(R.id.pos);
		holder.title = convertView.findViewById(R.id.title);
		holder.separator = convertView.findViewById(R.id.separator);
		holder.checkbox = convertView.findViewById(R.id.checkbox);
		holder.radio =  convertView.findViewById(R.id.radio);
		holder.pos.setText(String.valueOf(item.getPos()));
		if (item.getType() == DrawerItemType.STATE_NORMAL)
		{
			holder.title.setVisibility(View.VISIBLE);
			holder.title.setText(item.getTitle());
			holder.separator.setVisibility(View.GONE);
			holder.checkbox.setVisibility(View.GONE);
			holder.radio.setVisibility(View.GONE);
		}
		else if(item.getType() == DrawerItemType.STATE_SECTION)
		{
			holder.separator.setVisibility(View.VISIBLE);
			holder.separator.setText(item.getTitle());
			holder.title.setVisibility(View.GONE);
			holder.checkbox.setVisibility(View.GONE);
			holder.radio.setVisibility(View.GONE);
		}
		else if(item.getType() == DrawerItemType.STATE_CHECK)
		{
			holder.checkbox.setVisibility(View.VISIBLE);
			holder.checkbox.setText(item.getTitle());
			holder.checkbox.setChecked(item.isSelected());
			
			holder.separator.setVisibility(View.GONE);
			holder.title.setVisibility(View.GONE);
			holder.radio.setVisibility(View.GONE);
		}
		else if(item.getType() == DrawerItemType.STATE_RADIO)
		{
			holder.radio.setVisibility(View.VISIBLE);
			holder.radio.setText(item.getTitle());
			holder.radio.setChecked(item.isSelected());
			holder.separator.setVisibility(View.GONE);
			holder.checkbox.setVisibility(View.GONE);
			holder.title.setVisibility(View.GONE);
		}
		else
		{
			holder.title.setVisibility(View.GONE);
			holder.separator.setVisibility(View.GONE);
			holder.checkbox.setVisibility(View.GONE);
			holder.radio.setVisibility(View.GONE);
		}
			
		convertView.setTag(holder);

		return convertView;
	}
	


	@Override
	public int getViewTypeCount() { 
	    return 2;
	}
	
	@Override
	public int getItemViewType(int position) {
        DrawerItem item = getItem(position);
        assert item != null;
	    return item.getType().equals(DrawerItemType.STATE_SECTION) ? 0 : 1;
	}
	
	@Override
	public boolean isEnabled(int position) {
        DrawerItem item = getItem(position);
        assert item != null;
	    return !item.getType().equals(DrawerItemType.STATE_SECTION);
	}
	
}

class ViewHolder {
		TextView pos;
		TextView title;
		TextView separator;
		CheckedTextView checkbox;
		CheckedTextView radio;
}


