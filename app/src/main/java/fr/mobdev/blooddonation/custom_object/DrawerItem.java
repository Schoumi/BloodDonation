/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.custom_object;

import fr.mobdev.blooddonation.enums.DrawerItemType;

public class DrawerItem {
	private DrawerItemType type;
	private String title;
	private boolean selected;
	private static int pos = 0;
	private int id;
	
	public DrawerItem(DrawerItemType type, String title)
	{
		this.type = type;
		this.title = title;
		this.selected = false;
		id = pos++;
	}
	
	DrawerItemType getType() {
		return type;
	}
	public void setType(DrawerItemType type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public boolean isSelected()	{
		return selected;
	}
	
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	
	public int getPos()
	{
		return id;
	}

}
