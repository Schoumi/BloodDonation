/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.dialog.InformationDialog;
import fr.mobdev.blooddonation.dialog.PostDonationDialog;
import fr.mobdev.blooddonation.dialog.SupportDialog;
import fr.mobdev.blooddonation.fragment.AboutFragment;
import fr.mobdev.blooddonation.fragment.DonationFragment;
import fr.mobdev.blooddonation.fragment.DonationInfoFragment;
import fr.mobdev.blooddonation.fragment.InformationFragment;
import fr.mobdev.blooddonation.fragment.LicencesFragment;
import fr.mobdev.blooddonation.fragment.MapsFragment;
import fr.mobdev.blooddonation.fragment.NotificationFragment;
import fr.mobdev.blooddonation.fragment.PrefFragment;
import fr.mobdev.blooddonation.objects.BloodSite;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private MapsFragment.PostCodeUpdateListener postCodeUpdateListener;
    private int postCode;
    private List<Long> notifications;
    private Menu optionMenu;
    private Snackbar barWrite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        notifications = new ArrayList<>();
        postCode = -1;
        postCodeUpdateListener = new MapsFragment.PostCodeUpdateListener() {
            @Override
            public void onPostCodeChange(int postcode) {
                postCode = postcode;
            }
        };

        Toolbar toolbar = findViewById(R.id.history_toolbar);
        setSupportActionBar(toolbar);

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                setupToolbar(null);
            }
        });

        if (Database.getInstance(this).isFirstTime()) {
            showEditInformation(false);
        } else {
            showMaps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1 && grantResults.length == 2 && (grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment frag = manager.findFragmentById(R.id.fragment_container);
            if(frag instanceof MapsFragment) {
                ((MapsFragment)frag).startUpdateLocations();
            }
        } else if (requestCode == 1 && grantResults.length == 2) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("deactivate_localisation", true);
            editor.apply();
        }
        if(requestCode == 2 && grantResults.length == 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            barWrite = Snackbar.make(findViewById(R.id.fragment_container),R.string.need_write,Snackbar.LENGTH_INDEFINITE);
            barWrite.setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager manager = getSupportFragmentManager();
                    Fragment fragment = manager.findFragmentById(R.id.fragment_container);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(!shouldShowRequestPermissionRationale(permissions[0])) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 5);
                            dismissSnackBar();
                        } else {
                            if(fragment instanceof MapsFragment)
                                ((MapsFragment)fragment).askWritePermission();
                        }
                    } else {
                        if (fragment instanceof MapsFragment)
                            ((MapsFragment) fragment).askWritePermission();
                    }
                    dismissSnackBar();
                }
            });
            barWrite.show();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if(mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    private void showMaps() {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        FragmentManager manager = getSupportFragmentManager();
        Fragment maps = MapsFragment.newInstance(postCodeUpdateListener);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container,maps);
        transaction.commit();
        setupToolbar(maps);
        if(!Database.getInstance(this).hasShownSupport()){
            SupportDialog dialog = new SupportDialog();
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(),null);
        }
    }

    private void showEditInformation(boolean modify) {
        final FragmentManager manager = getSupportFragmentManager();

        Fragment information = InformationFragment.newInstance(modify, new InformationFragment.InformationListener() {
            @Override
            public void onValidate(boolean isFirstTime) {
                if(isFirstTime)
                    showMaps();
                else
                    manager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                View view = getCurrentFocus();
                if(view == null)
                    view = new View(MainActivity.this);
                if(inputMethodManager == null)
                    return;
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
            }
        });
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container,information);
        if(modify)
            transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(information);
    }

    private void showAbout() {
        FragmentManager manager = getSupportFragmentManager();

        Fragment about = AboutFragment.newInstance(new AboutFragment.OnLicenceClickListener() {
            @Override
            public void onLicenceClick() {
                showLicence();
            }
        });
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, about);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(about);
    }

    private void showDonation() {
        FragmentManager manager = getSupportFragmentManager();

        Fragment donation = DonationFragment.newInstance(new DonationFragment.OnDeletionModeListener() {
            @Override
            public void onDeleteModeChange(boolean activeDelete) {
                MenuItem delItem = optionMenu.findItem(R.id.action_del);
                MenuItem addItem = optionMenu.findItem(R.id.action_add);
                if(activeDelete) {
                    delItem.setVisible(true);
                    addItem.setVisible(false);
                }
                else {
                    delItem.setVisible(false);
                    addItem.setVisible(true);
                }

            }
        });
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, donation);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(donation);
    }

    private void showDonationInfo() {
        FragmentManager manager = getSupportFragmentManager();

        Fragment donationInfo = new DonationInfoFragment();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, donationInfo);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(donationInfo);
    }

    private void showLicence() {
        FragmentManager manager = getSupportFragmentManager();

        Fragment licenceFragment = new LicencesFragment();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, licenceFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(licenceFragment);
    }

    private void showNotifications() {
        FragmentManager manager = getSupportFragmentManager();

        Fragment notifications = new NotificationFragment();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, notifications);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(notifications);
    }

    private void showPrefs() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment prefs = PrefFragment.newInstance(new PrefFragment.OnEditPersonnalInfoListener() {
            @Override
            public void onEditPesonnalInfoClick() {
                showEditInformation(true);
            }
        });
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, prefs);
        transaction.addToBackStack(null);
        transaction.commit();
        setupToolbar(prefs);
    }

    void setupToolbar(Fragment frag) {
        if (frag == null) {
            FragmentManager manager = getSupportFragmentManager();
            frag = manager.findFragmentById(R.id.fragment_container);
        }
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;

        if(optionMenu == null)
            return;

        MenuItem notif = optionMenu.findItem(R.id.action_notif);
        notif.setVisible(false);
        MenuItem add = optionMenu.findItem(R.id.action_add);
        add.setVisible(false);
        MenuItem del = optionMenu.findItem(R.id.action_del);
        del.setVisible(false);
        MenuItem help = optionMenu.findItem(R.id.action_help);
        help.setVisible(false);

        if(frag instanceof DonationFragment) {
            actionBar.setTitle(R.string.donations);
            DonationFragment fragment = (DonationFragment) frag;
            if(fragment.isInDeletionMode()) {
                del.setVisible(true);
                add.setVisible(false);
            } else {
                add.setVisible(true);
                del.setVisible(false);
            }
        } else if(frag instanceof PrefFragment) {
            actionBar.setTitle(R.string.preference);
        } else if(frag instanceof MapsFragment) {
            actionBar.setTitle(R.string.app_name);
            notif.setVisible(true);
        } else if(frag instanceof InformationFragment) {
            actionBar.setTitle(R.string.info_title);
            help.setVisible(true);
        } else if(frag instanceof AboutFragment) {
            actionBar.setTitle(R.string.about);
        } else if(frag instanceof DonationInfoFragment) {
            actionBar.setTitle(R.string.donation_info);
        } else if(frag instanceof NotificationFragment) {
            actionBar.setTitle(R.string.notif_title);
        } else if(frag instanceof LicencesFragment) {
            actionBar.setTitle(R.string.licence);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
            case R.id.nav_my_donation:
                showDonation();
                dismissSnackBar();
                break;
            case R.id.nav_user_information:
                showDonationInfo();
                dismissSnackBar();
                break;
            case R.id.nav_legend:
                openLegendDialog();
                break;
            case R.id.nav_preferences:
                showPrefs();
                dismissSnackBar();
                break;
            case R.id.nav_post_don:
                openPostDonationDialog();
                break;
            case R.id.nav_about:
                showAbout();
                dismissSnackBar();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void dismissSnackBar() {
        if(barWrite != null) {
            barWrite.dismiss();
            barWrite = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        optionMenu = menu;

        MenuItem item = menu.findItem(R.id.action_notif);
        ImageButton iv = (ImageButton) getLayoutInflater().inflate(R.layout.notif,null);
        iv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final int MENU_ITEM = Menu.FIRST;
                final int SHOW_ALL = MENU_ITEM + 3;
                final PopupMenu menu = new PopupMenu(MainActivity.this,v);
                int i = 0;
                List<BloodSite> sites = Database.getInstance(getApplicationContext()).getNotifications();
                notifications.clear();
                for(BloodSite site : sites)
                {
                    if(i == 3)
                        break;
                    DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
                    df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
                    String date = df.format(site.getDate().getTime());
                    String notif = date + " - " + site.getCityName();
                    menu.getMenu().add(0,MENU_ITEM + i, Menu.NONE,notif);
                    notifications.add(site.getDbId());
                    i++;
                }
                if(sites.size() > 3)
                    menu.getMenu().add(0,SHOW_ALL, Menu.NONE,getString(R.string.show_all));
                else if (sites.size() == 0)
                    menu.getMenu().add(0,MENU_ITEM,Menu.NONE,getString(R.string.no_notif));
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        int menuId = Menu.FIRST;
                        int id = item.getItemId() - menuId;
                        if(notifications.size() > 0)
                        {
                            if(id != 3)
                            {
                                long siteId = notifications.get(id);
                                Bundle args = new Bundle();
                                args.putLong("siteId", siteId);
                                InformationDialog dialog = new InformationDialog();
                                dialog.setArguments(args);
                                dialog.show(getSupportFragmentManager(), "information dialog");
                                notifications.remove(id);
                                List<Long> notificationsToRemove = new ArrayList<>();
                                notificationsToRemove.add(siteId);
                                Database.getInstance(getApplicationContext()).skipNotifications(notificationsToRemove);
                                menu.getMenu().removeItem(id);
                            }
                            else
                            {
                                showNotifications();
                            }
                        }
                        return true;
                    }
                });
                menu.show();
            }
        });
        item.setActionView(iv);
        setupToolbar(null);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        FragmentManager manager = getSupportFragmentManager();
        DonationFragment donationFragment;

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                manager.popBackStack();
                return true;
            case R.id.action_add:
                donationFragment = (DonationFragment) manager.findFragmentById(R.id.fragment_container);
                donationFragment.addDonation();
                return true;
            case R.id.action_del:
                donationFragment = (DonationFragment) manager.findFragmentById(R.id.fragment_container);
                donationFragment.askRemoveDonation();
                return true;
            case R.id.action_help:
                showHelpDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("InflateParams")
    private void showHelpDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.help_title))
                .setView(
                        getLayoutInflater().inflate(R.layout.help_dialog, null))
                .setCancelable(true)
                .setNegativeButton(R.string.close,
                        new AlertDialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
        builder.show();
    }

    @SuppressLint("InflateParams")
    private void openLegendDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.legend_dialog, null);

        builder.setView(view)
                .setTitle(R.string.legend)
                .setNegativeButton(R.string.close,
                        new AlertDialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();

                            }
                        }).show();
    }

    private void openPostDonationDialog() {
        PostDonationDialog dialog = new PostDonationDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("depId", postCode);

        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "post donation phone number");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        FragmentManager manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentById(R.id.fragment_container);
        DonationFragment donationFragment = null;
        if (frag instanceof DonationFragment)
            donationFragment = (DonationFragment) frag;

        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(donationFragment != null && donationFragment.isInDeletionMode()) {
            donationFragment.cancelDeletionMode();
        } else {
            super.onBackPressed();
            setupToolbar(null);
        }
    }
}
