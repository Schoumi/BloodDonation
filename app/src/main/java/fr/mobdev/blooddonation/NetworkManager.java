/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.util.Log;

import com.car2go.maps.model.LatLng;

import fr.mobdev.blooddonation.enums.Country;
import fr.mobdev.blooddonation.enums.SiteType;
import fr.mobdev.blooddonation.objects.BloodSite;

public class NetworkManager {
	//TODO Complete rework with thread and message Queue
	private NetworkListener listener;
	private static NetworkManager instance;
	private Context context;
	private static int id;
	private List<Integer> idList; 
	
	private NetworkManager(NetworkListener listener,Context context)
	{
		this.listener = listener;
		this.context = context;
		idList = new ArrayList<>();
	}
	
	public static NetworkManager getInstance(NetworkListener listener, Context context){
		if (instance == null)
			instance = new NetworkManager(listener,context);
		if(listener != instance.getListener())
			instance.setListener(listener);
		return instance;
	}
	
	private NetworkListener getListener()
	{
		return listener;
	}
	
	private void setListener(NetworkListener listener)
	{
		this.listener = listener;
	}
	
	public void loadFromNet(final Country ctr, final LatLng nECorner, final LatLng sWCorner)
	{
		if(!isConnectedToInternet(context))
			return;
		new Thread(new Runnable() {

			@Override
			public void run() {
				List<BloodSite> sites = null;
				int refreshId = id++;
				idList.add(refreshId);
				if(ctr == Country.FRANCE)
				{
					sites  = loadFrenchData(nECorner,sWCorner);
				}
				if(idList.size() != 0 && idList.get(idList.size()-1) == refreshId)
				{
					listener.bloodSiteListChanged(sites);
				}
			}
		}).start();
	}

	private List<BloodSite> loadFrenchData(LatLng nECorner, LatLng sWCorner)
	{
		List<BloodSite> siteList = new ArrayList<>();
		String urlstr = "http://carte.dondusang.com/gmap_regionchanged.php?"
				+"nelat="+nECorner.latitude
				+"&nelon="+nECorner.longitude
				+"&swlat="+sWCorner.latitude
				+"&swlon="+sWCorner.longitude
				+"&marker=2&fb=facebook&t1=0&t2=10";
		//getData
		URL url = null;
		try {
			url = new URL(urlstr);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		URLConnection connect;
		InputStream stream = null;
		try {
			if(isConnectedToInternet(context))
			{
				connect = url.openConnection();
				stream = connect.getInputStream();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if(stream != null)
		{
			InputStreamReader isr = new InputStreamReader(stream);
			BufferedReader br = new BufferedReader(isr);
			boolean isReading = true;
			String data;
			String jsonStr = "";
			do
			{
				try {
					data = br.readLine();
					if(data != null)
						jsonStr+=data;
					else
						isReading = false;
					if(idList.get(idList.size()-1) != id-1)
					{
						return null;
					}
	
				} catch (IOException e) {
					e.printStackTrace();
					isReading = false;
				}
	
	
			}while(isReading);
			//parseJSon
	
			try {
				JSONObject rootObject = new JSONObject(jsonStr); // Parse the JSON to a JSONObject
				int nbResults = rootObject.getInt("num_results");
				for(int j=0; j < nbResults; j++) { // Iterate each element in the elements array
					JSONObject site =  rootObject.getJSONObject(String.valueOf(j)); // Get the element object
					
					long siteId = -1; //obtain with c_id
					Calendar date = null;//obtain with c_date
					String siteName = null; //obtain with lp_libconv
					String address = null;//obtain by parsing text
					String details = null;//obtain by parsing text
					String cityName = null;//obtain with ville
					String phone = null;//obtain with num_tel
					String mail = null;
					
					double latitude = site.getDouble("lat"); // Get duration sub object
					double longitude = site.getDouble("lon"); // Get distance sub object
					String text = site.getString("text");
					String ico = site.getString("icon");
					
					if(site.has("c_id"))
						siteId = site.getInt("c_id");
					if(site.has("lp_libconv"))
						siteName = site.getString("lp_libconv");
					if(site.has("c_date") && site.getLong("c_date") > 0)
					{
						date = Calendar.getInstance();
						date.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
						date.setTimeInMillis(site.getLong("c_date")*1000);	
					}
					if(site.has("ville"))
						cityName = site.getString("ville");
					if(site.has("num_tel"))
						phone = site.getString("num_tel");
					if(site.has("adr_mail"))
						mail = site.getString("adr_mail");

					SiteType type;
					if(ico.startsWith("efs"))
						type = SiteType.efs;
					else if(ico.startsWith("rou"))
						type = SiteType.mobile_blood;
					else if(ico.startsWith("jau"))
						type = SiteType.mobile_plasma;
					else
						type = SiteType.mobile_blood_plasma;
					
					//parse text for adress and details
					
					text = text.replaceAll("<br>", "\n");
					text = text.replaceAll("<br/>","\n");
					text = text.replaceAll("<br >", "\n");
					text = text.replaceAll("<br />","\n");
					text = text.replaceAll("<b>","");
					text = text.replaceAll("</b>","");
					text = text.replaceAll("&aacute;", "á");
					text = text.replaceAll("&agrave;", "à");
					text = text.replaceAll("&acirc;", "â");
					text = text.replaceAll("&atild;", "ã");
					text = text.replaceAll("&auml;", "ä");
					text = text.replaceAll("&eacute;", "é");
					text = text.replaceAll("&egrave;", "è");
					text = text.replaceAll("&ecirc;", "ê");
					text = text.replaceAll("&euml;", "ë");
					text = text.replaceAll("&igrave;", "ì");
					text = text.replaceAll("&iacute;", "í");
					text = text.replaceAll("&icirc;", "î");
					text = text.replaceAll("&iuml;", "ï");
					text = text.replaceAll("&oacute;", "ó");
					text = text.replaceAll("&ograve;", "ò");
					text = text.replaceAll("&ocirc;", "ô");
					text = text.replaceAll("&otild;", "õ");
					text = text.replaceAll("&ouml;", "ö");
					text = text.replaceAll("&ugrave;", "ù");
					text = text.replaceAll("&uacute;", "ú");
					text = text.replaceAll("&ucirc;", "û");
					text = text.replaceAll("&uuml;", "ü");
					text = text.replaceAll("&quot;", "\"");
					text = text.replaceAll("&#039;", "'");
					text = text.replaceAll("\r", "\n");
					text = text.replaceAll("\n ", "\n");
					String balise = "<div id=\"gbadresse\">";
					if(site.has("lp_ad3"))
					{
						text = text.substring(text.indexOf(">")+1);
						details = text.substring(0,text.indexOf("</div>"));
						text = text.substring(text.indexOf("</div>"));
						
						text = text.substring(text.indexOf(balise)+balise.length());
						address = text.substring(0, text.indexOf("</div>"));
					}
					else if(site.has("lp_libconv"))
					{
						text = text.replaceAll("<span style=\"color:#C62B2B\">", "");
						text = text.replaceAll("</span>", "");
						String div = "<div>";
						text = text.substring(text.indexOf(div)+div.length());
						text = text.substring(text.indexOf(div)+div.length());
						details = text.substring(0,text.indexOf("</div>"));
						text = text.substring(text.indexOf(balise)+balise.length());
						address = text.substring(0, text.indexOf("</div>"));
					}
	
					BloodSite newSite = new BloodSite(Country.FRANCE);
					newSite.setSiteId(siteId);
					newSite.setDate(date);
					newSite.setSiteName(siteName);
					newSite.setLoc(new LatLng(latitude,longitude));
					newSite.setType(type);
					newSite.setCityName(cityName);
					newSite.setPhone(phone);
					newSite.setMail(mail);
					newSite.setAddress(address);
					newSite.setDetails(details);
					
					if(siteList.contains(newSite))
					{
						String begin = newSite.getDetails();
						begin = begin.substring(0,begin.indexOf("à"));
						begin = begin.substring(begin.indexOf("h")-2, begin.indexOf("h")+3);
						int newBeginHour = Integer.valueOf(begin.substring(0,2)) * 1000 * 60 *60 + Integer.valueOf(begin.substring(3,5))*1000*60;
						
						BloodSite oldSite = siteList.get(siteList.indexOf(newSite));
						begin = oldSite.getDetails();
						begin = begin.substring(0,begin.indexOf("à"));
						begin = begin.substring(begin.indexOf("h")-2, begin.indexOf("h")+3);
						int beginHour = Integer.valueOf(begin.substring(0,2)) * 1000 * 60 *60 + Integer.valueOf(begin.substring(3,5))*1000*60;
						
						if(beginHour > newBeginHour)
						{
							String newDetails = oldSite.getDetails();
							newDetails = newDetails.substring(newDetails.lastIndexOf("de"),newDetails.lastIndexOf("h")+3);
							newSite.setDetails(newSite.getDetails()+newDetails);
							siteList.remove(oldSite);
							siteList.add(newSite);
						}
						else
						{
							String newDetails = newSite.getDetails();
							newDetails = newDetails.substring(newDetails.lastIndexOf("de"),newDetails.lastIndexOf("h")+3);
							oldSite.setDetails(oldSite.getDetails()+newDetails);
						}
					}
					else
					{
						siteList.add(newSite);
					}
					
				}
			} catch (JSONException e) {
				// JSON Parsing error
				e.printStackTrace();
			}
		}

		//build Blood Site and construct list
		return siteList;
	}
	
	private boolean isConnectedToInternet(Context context) 
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null) 
		{
			State networkState = networkInfo.getState();
			if (networkState.equals(State.CONNECTED)) 
			{
				return true;
			}
		}
		return false;
	}
}