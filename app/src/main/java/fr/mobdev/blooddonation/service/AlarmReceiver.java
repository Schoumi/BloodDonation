/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.service;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.NetworkListener;
import fr.mobdev.blooddonation.NetworkManager;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.activity.MainActivity;
import fr.mobdev.blooddonation.enums.Country;
import fr.mobdev.blooddonation.enums.SiteType;
import fr.mobdev.blooddonation.objects.BloodSite;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.preference.PreferenceManager;

import com.car2go.maps.model.LatLng;

public class AlarmReceiver extends BroadcastReceiver {

	private static final long two_month_millis =  5184000000L;
	private int NOTIFICATION = R.string.app_name;
	private static List<BloodSite> newSite = new ArrayList<>();
	private static List<LatLng> NECornerList = new ArrayList<>();
	@Override
	public void onReceive(Context context, Intent intent) {
		if(NECornerList.size() > 0)
		{
			NECornerList.clear();
			newSite.clear();
		}

		int service = intent.getIntExtra("service", -1);
		if(service == 1)
		{
			CheckDonation(context);
			BootReceiver.startNextChecking(context, true);
		}
		else
		{
			NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
			NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);


			boolean no_notify = !prefs.getBoolean("notif_activ", true);

			if(newSite.size() == 0 || no_notify)
				return;

			for(BloodSite site: newSite)
			{
				String text="";
				Calendar today = Calendar.getInstance();
				today.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
				today.set(Calendar.HOUR_OF_DAY, 0);
				today.set(Calendar.MINUTE, 0);
				today.set(Calendar.SECOND, 0);
				today.set(Calendar.MILLISECOND, 0);
				if(site.getDate().getTimeInMillis() >= today.getTimeInMillis())
				{
					DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
					df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
					String date = df.format(site.getDate().getTime());
					text += date;
					text += " - ";
					text += site.getCityName();
					text += "\n";
				}
				inboxStyle.addLine(text);
			}
			String title = String.valueOf(newSite.size())+" ";

			Calendar cal = Database.getInstance(context).getLastDonationDate();

			boolean neighbor = (cal != null && Calendar.getInstance().getTimeInMillis() - cal.getTimeInMillis() < two_month_millis);



			if(newSite.size() == 1 && !neighbor)
				title+="don près de chez vous";
			else if(newSite.size() != 1 && !neighbor)
				title+="dons près de chez vous";
			else if(newSite.size() == 1 && neighbor)
				title = "Parlez de ce don à vos amis";
			else
				title = "Parlez de ces "+ title + "dons à vos amis";
			inboxStyle.setSummaryText(context.getText(R.string.app_name));
			inboxStyle.setBigContentTitle(title);
			builder.setStyle(inboxStyle);
			builder.setContentText(title);
			builder.setContentTitle(context.getString(R.string.app_name));
			builder.setSmallIcon(R.drawable.ic_launcher);
			builder.setAutoCancel(true);
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			Intent newIntent = new Intent(context, MainActivity.class);

			stackBuilder.addParentStack(MainActivity.class);
			stackBuilder.addNextIntent(newIntent);
			PendingIntent resultPendingIntent =  stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT);
			builder.setContentIntent(resultPendingIntent);
			nm.cancel(NOTIFICATION);
			nm.notify(NOTIFICATION, builder.build());
			newSite.clear();
		}
	}

	private void CheckDonation(final Context context)
	{
		LatLng loc = Database.getInstance(context).getUserLastLocation();
		populateNeCornerList(loc,context);
		NetworkListener listener = new NetworkListener() {

			@Override
			public void bloodSiteListChanged(List<BloodSite> sites) {

				Database.getInstance(context).addSitesToDb(sites);

				if(NECornerList.size() == 0)
					prepareNotifications(context);

				if(NECornerList.size() > 0)
				{
					LatLng NECorner = NECornerList.get(0);
					LatLng SWCorner = new LatLng(NECorner.latitude - 0.1, NECorner.longitude - 0.1);
					NetworkManager.getInstance(this, context).loadFromNet(Country.FRANCE, NECorner, SWCorner);
					NECornerList.remove(0);
				}
			}
		};
		if(NECornerList.size() > 0)
		{
			LatLng NECorner = NECornerList.get(0);
			LatLng SWCorner = new LatLng(NECorner.latitude - 0.1, NECorner.longitude - 0.1);
			NetworkManager.getInstance(listener, context).loadFromNet(Country.FRANCE, NECorner, SWCorner);
			NECornerList.remove(0);
		}
	}

	public static void prepareNotifications(Context context)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.DAY_OF_YEAR,cal.get(Calendar.DAY_OF_YEAR)+14);
		List<BloodSite> sites = Database.getInstance(context).getBloodSites(-1);
		checkForProximity(sites, context);
		checkForDate(sites, context);
		List<Long> notifications = Database.getInstance(context).getAllNotifications();
		List<Long> newNotifications = new ArrayList<>();
		for(BloodSite site : sites)
		{
			if(site.getDate().compareTo(cal) < 0)
				newNotifications.add(site.getDbId());
		}

		newNotifications.removeAll(notifications);
		Database.getInstance(context).addNotifications(newNotifications);
		notifyUser(context,newNotifications);
	}

	public static void notifyUser(Context context, List<Long> sites)
	{
		newSite.clear();
		for(Long id : sites)
		{
			newSite.addAll(Database.getInstance(context).getBloodSites(id));
		}
		if(newSite.size() > 0)
		{
			AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, AlarmReceiver.class);
			intent.putExtra("service", -1);
			intent.addCategory("DonDuSang notifications");
			PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
			am.cancel(contentIntent);
			am.set(AlarmManager.RTC, System.currentTimeMillis(), contentIntent);
		}
	}
    
    /*
    public static void addNewSiteToNotify(Context context)
    {
		newSite.addAll(Database.getInstance(context).getNotifications());
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		if(notify && prefs.getBoolean("service_activ", true))
		{
			sites.clear();
			/************************************************************
			LatLng loc = new LatLng(45.7579555,4.8351209);
			BloodSite newSiteItem = new BloodSite(Country.FRANCE);//in limits
			newSiteItem.setCityName("Lyon in limits");
			newSiteItem.setType(SiteType.mobile_blood);
			Calendar date = Calendar.getInstance();
			date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH)+2);
			newSiteItem.setLoc(loc);
			newSiteItem.setDate(date);
			sites.add(newSiteItem);
			
			newSiteItem = new BloodSite(Country.FRANCE);//in limits but yesterday
			newSiteItem.setCityName("Lyon old");
			newSiteItem.setType(SiteType.mobile_blood);
			date = Calendar.getInstance();
			date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH)-2);
			newSiteItem.setLoc(new LatLng(loc.latitude+0.01, loc.longitude+0.01));
			newSiteItem.setDate(date);
			sites.add(newSiteItem);
			
			newSiteItem = new BloodSite(Country.FRANCE);//out of limits
			newSiteItem.setCityName("Lyon out of");
			newSiteItem.setType(SiteType.mobile_blood);
			date = Calendar.getInstance();
			date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH)+2);
			newSiteItem.setLoc(new LatLng(loc.latitude+2.00, loc.longitude+2.00));
			newSiteItem.setDate(date);
			sites.add(newSiteItem);
			checkForProximity(sites,context);
			checkForDate(sites, context);
			newSite.addAll(sites);
			***********************************************************
			
			if(newSite.size() > 0)
			{
				AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
				Intent intent = new Intent(context, AlarmReceiver.class);
		        intent.putExtra("service", -1);
		        intent.addCategory("DonDuSang notifications");
				PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
				am.cancel(contentIntent);
				am.set(AlarmManager.RTC, System.currentTimeMillis(), contentIntent);
			}
		}
		
    }*/

	private static void checkForDate(List<BloodSite> sites, Context context) {
		List<BloodSite> removeList = new ArrayList<>();
		for(BloodSite site: sites)
		{
			Calendar today = Calendar.getInstance();
			today.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.set(Calendar.MILLISECOND, 0);
			if(site.getDate().getTimeInMillis() < today.getTimeInMillis())
			{
				removeList.add(site);
			}
		}
		sites.removeAll(removeList);
	}

	private void populateNeCornerList(LatLng loc, Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int distanceTo = Integer.parseInt(prefs.getString("distance_to_loc", "5"));
		int number = (int)(distanceTo/10.0f*2.0f);
		if(number < 2)
			number = 2;

		for(int i = 0; i < number; i++)
		{
			for(int j = 0; j < number; j++)
			{
				int lon;
				int lat;
				if(i % 2 == 0)
					lat = (-number)/2;
				else
					lat = (number+1)/2;

				if(j % 2 == 0)
					lon = (-number)/2;
				else
					lon = (number+1)/2;

				NECornerList.add(new LatLng(lat * 0.1 + loc.latitude, lon * 0.1 + loc.longitude));//0.1° ~= 10km
			}
		}
	}

	private static void checkForProximity(List<BloodSite> sites, Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int distanceTo = Integer.parseInt(prefs.getString("distance_to_loc", "5"));
		LatLng lat = Database.getInstance(context).getUserLastLocation();
		Location loc = new Location("");
		loc.setLatitude(lat.latitude);
		loc.setLongitude(lat.longitude);

		ArrayList<BloodSite> removeList = new ArrayList<>();

		for(BloodSite site : sites)
		{
			Location siteLoc = new Location("");
			siteLoc.setLatitude(site.getLoc().latitude);
			siteLoc.setLongitude(site.getLoc().longitude);
			if(loc.distanceTo(siteLoc)/1000 > distanceTo || site.getType().equals(SiteType.efs))
				removeList.add(site);
		}

		sites.removeAll(removeList);
	}
}
