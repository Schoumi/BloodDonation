/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class BootReceiver extends BroadcastReceiver {

	private static final int jumpTime = 24 * 60 * 60 * 1000;
	private static final int bootJumpTIme = 2 * 60 * 1000;
	
	@Override
	public void onReceive(Context context, Intent intent) {
	    startNextChecking(context,false);
	}
	
	public static void startNextChecking(Context context, boolean jump) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if(prefs.getBoolean("service_activ", true))
		{
			AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, AlarmReceiver.class);
	        intent.putExtra("service", 1);
	        intent.addCategory("DonDuSang service 1");
			PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
			am.cancel(contentIntent);
			if(!jump)
				am.set(AlarmManager.RTC, System.currentTimeMillis() + bootJumpTIme, contentIntent);
			else
				am.set(AlarmManager.RTC, System.currentTimeMillis()+jumpTime, contentIntent);
		}
	}
}
