/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import fr.mobdev.blooddonation.enums.Country;
import fr.mobdev.blooddonation.enums.DonationType;
import fr.mobdev.blooddonation.enums.SiteType;
import fr.mobdev.blooddonation.objects.BloodSite;
import fr.mobdev.blooddonation.objects.Donation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.car2go.maps.model.LatLng;


public class Database extends SQLiteOpenHelper {

    private static Database instance;

    public static Database getInstance(Context context)
    {
        if(instance == null)
            instance = new Database(context, "BloodDonation.db", null, 4);
        return instance;
    }

    private Database(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table if not exists user (" +
                "id integer primary key autoincrement, name varchar(1024), city varchar(1024), postal varchar(10), gender INTEGER, blood varchar(10), D INTEGER, C INTEGER, E INTEGER, min_c INTEGER, min_e INTEGER, K INTEGER" +
                ",latitude REAL, longitude REAL, show_support INTEGER);");
        db.execSQL("Create table if not exists site (" +
                "id integer primary key autoincrement, country INTEGER, address varchar(1024), city varchar(1024), details varchar(4096), mail varchar(1024), phone varchar(1024),"
                +"siteName varchar(1024), siteId INTEGER, date INTEGER, latitude REAL, longitude REAL, type INTEGER, schedule INTEGER" +
                ");");
        db.execSQL("Create table if not exists donation (" +
                "id integer primary key autoincrement, date INTEGER, siteId INTEGER, city varchar(1024), donation_type INTEGER" +
                ");");
        db.execSQL("Create table if not exists notification (" +
                "id integer primary key autoincrement, siteId INTEGER, skip INTEGER "  +
                ");");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("Create table if not exists notification (" +
                "id integer primary key autoincrement, siteId INTEGER, skip INTEGER "  +
                ");");
        if(oldVersion == 2 && newVersion == 3)
        {
            db.execSQL("Alter Table notification Add Column skip INTEGER;");
        }
        if(oldVersion <= 3) {
            db.execSQL("Alter Table user Add Column show_support INTEGER;");

                String[] column = {"id"};
                Cursor cursor = db.query("user",column,null,null,null,null,null);
                if(cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    int userId;
                    userId = cursor.getInt(0);
                    cursor.close();
                    String whereClause = "id = ?";
                    String[] whereArgs = new String[1];
                    whereArgs[0] = String.valueOf(userId);
                    ContentValues values = new ContentValues();
                    values.put("show_support", false);
                    db.update("user", values, whereClause, whereArgs);
                }
        }

    }

    public void updateUserInfo(String name, boolean isMale, String city, String postal, String blood, boolean D, boolean C, boolean E, boolean c, boolean e, boolean K) {
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("city", city);
        values.put("postal", postal);
        values.put("blood", blood);
        values.put("gender", isMale);
        values.put("D",D);
        values.put("C",C);
        values.put("E",E);
        values.put("min_c",c);
        values.put("min_e",e);
        values.put("K",K);
        values.put("latitude", 0);
        values.put("longitude", 0);
        if(!isFirstTime())
        {
            int userId;
            String[] column = {"id"};
            Cursor cursor = getReadableDatabase().query("user",column,null,null,null,null,null);
            cursor.moveToFirst();
            userId = cursor.getInt(0);
            String whereClause = "id = ?";
            String[] whereArgs = new String[1];
            whereArgs[0] = String.valueOf(userId);
            getWritableDatabase().update("user", values, whereClause, whereArgs);
            cursor.close();
        }
        else
        {
            values.put("show_support",false);
            getWritableDatabase().insert("user", null, values);
        }

    }

    public void updateUserLoc(double latitude, double longitude)
    {
        ContentValues values = new ContentValues();
        values.put("latitude", latitude);
        values.put("longitude", longitude);
        int userId;
        Cursor cursor = getReadableDatabase().query("user",null,null,null,null,null,null);
        cursor.moveToFirst();
        userId = cursor.getInt(0);
        String whereClause = "id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(userId);
        getWritableDatabase().update("user", values, whereClause, whereArgs);
        cursor.close();
    }

    public boolean isFirstTime() {
        Cursor cursor = getReadableDatabase().query("user",null,null,null,null,null,null);
        if(cursor.getCount() != 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void addSitesToDb(List<BloodSite> sites) {
        if(sites == null)
            return;
        //remove existing site in the list
        List<BloodSite> oldList = getBloodSites(-1);
        List<BloodSite> removeList = new ArrayList<>();
        for(BloodSite site : sites)
        {
            if(oldList.contains(site))
            {
                removeList.add(site);
            }
            else
            {
                ContentValues values = new ContentValues();
                values.put("address", site.getAddress());
                values.put("city", site.getCityName());
                values.put("details", site.getDetails());
                values.put("mail", site.getMail());
                values.put("phone", site.getPhone());
                values.put("siteName", site.getSiteName());
                values.put("siteId", site.getSiteId());
                values.put("country", site.getCountry().ordinal());
                if(site.getDate()!=null)
                    values.put("date", site.getDate().getTimeInMillis());
                else
                    values.put("date", -1);
                values.put("latitude", site.getLoc().latitude);
                values.put("longitude", site.getLoc().longitude);
                values.put("type", site.getType().ordinal());
                getWritableDatabase().insert("site", null, values);
            }
        }
        sites.removeAll(removeList);
    }

    public List<Donation> getDonations() {
        List<Donation> donations = new ArrayList<>();
        String orderBy = "date DESC";
        Cursor cursor = getReadableDatabase().query("donation", null, null, null, null, null, orderBy);
        while(cursor.moveToNext())
        {
            int col = 0;
            int donationId = cursor.getInt(col++);
            long date = cursor.getLong(col++);
            int siteId = cursor.getInt(col++);
            String city_name = cursor.getString(col++);
            int donation_type = cursor.getInt(col);

            Calendar cal = Calendar.getInstance();
            cal.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
            cal.setTimeInMillis(date);

            Donation donation = new Donation(DonationType.values()[donation_type], cal, siteId, donationId, city_name);
            donations.add(donation);
        }
        cursor.close();
        return donations;
    }

    public void deleteDonation(List<Donation> deletedList) {
        for(Donation donation : deletedList)
        {
            String whereClause = "id = ?";
            String[] whereArgs = new String[1];
            whereArgs[0] = String.valueOf(donation.getDonationId());
            getWritableDatabase().delete("donation", whereClause, whereArgs);
        }
    }

    public void addDonation(Donation donation) {
        ContentValues values = new ContentValues();
        values.put("siteId", donation.getSiteId());
        values.put("date", donation.getDate().getTimeInMillis());
        values.put("donation_type", donation.getDonationType().ordinal());
        values.put("city", donation.getCityName());
        getWritableDatabase().insert("donation", null, values);
    }

    public HashMap<String, Object> getUserInformation()
    {
        HashMap<String, Object> maps = new HashMap<>();
        if(!isFirstTime())
        {
            Cursor cursor = getReadableDatabase().query("user",null,null,null,null,null,null);
            cursor.moveToFirst();
            int col = 1;
            maps.put("UserName", cursor.getString(col++));//1
            maps.put("CityName", cursor.getString(col++));//2
            maps.put("Postal", cursor.getString(col++));//3
            maps.put("Gender", cursor.getInt(col++));//4
            maps.put("BloodGroup", cursor.getString(col++));//5
            maps.put("D", cursor.getInt(col++));
            maps.put("C", cursor.getInt(col++));
            maps.put("E", cursor.getInt(col++));
            maps.put("c", cursor.getInt(col++));
            maps.put("e", cursor.getInt(col++));
            maps.put("K", cursor.getInt(col));
            cursor.close();
        }
        return maps;
    }

    public List<BloodSite> getBloodSites(long siteId) {
        ArrayList<BloodSite> list = new ArrayList<>();
        String whereClause = null;
        String[] whereArgs = null;
        if(siteId != -1)
        {
            whereClause = "id = ?";
            whereArgs = new String[1];
            whereArgs[0] = String.valueOf(siteId);
        }


        Cursor cursor = getReadableDatabase().query("site", null, whereClause, whereArgs, null, null, null);
        while(cursor.moveToNext())
        {
            int col = 0;
            long dbId = cursor.getLong(col++);

            int ordinal =cursor.getInt(col++);
            BloodSite site = new BloodSite(Country.values()[ordinal]);
            site.setDbId(dbId);
            site.setAddress(cursor.getString(col++));
            site.setCityName(cursor.getString(col++));
            site.setDetails(cursor.getString(col++));
            site.setMail(cursor.getString(col++));
            site.setPhone(cursor.getString(col++));
            site.setSiteName(cursor.getString(col++));
            site.setSiteId(cursor.getLong(col++));
            long time = cursor.getLong(col++);
            if(time == -1)
                site.setDate(null);
            else
            {
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
                date.setTimeInMillis(time);
                site.setDate(date);
            }
            double lat = cursor.getDouble(col++);
            double lon = cursor.getDouble(col++);
            site.setLoc(new LatLng(lat, lon));
            site.setType(SiteType.values()[cursor.getInt(col)]);
            list.add(site);
        }
        cursor.close();
        return list;
    }

    public BloodSite getPastUnregisteredDonation() {

        String whereClause = "schedule = ? and date < ?";
        String[] whereArgs = new String[2];
        whereArgs[0] = String.valueOf(1);

        Calendar dateTime = Calendar.getInstance();
        dateTime.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        dateTime.set(Calendar.HOUR_OF_DAY, 0);
        dateTime.set(Calendar.MILLISECOND, 0);
        dateTime.set(Calendar.MINUTE, 0);
        dateTime.set(Calendar.SECOND, 0);
        whereArgs[1] = String.valueOf(dateTime.getTimeInMillis());
        String orderBy = "date ASC";

        BloodSite site = null;

        Cursor cursor = getReadableDatabase().query("site", null, whereClause, whereArgs, null, null, orderBy);
        if(cursor.moveToFirst())
        {
            int col = 0;
            long dbId = cursor.getLong(col++);

            int ordinal = cursor.getInt(col++);
            site = new BloodSite(Country.values()[ordinal]);
            site.setDbId(dbId);
            site.setAddress(cursor.getString(col++));
            site.setCityName(cursor.getString(col++));
            site.setDetails(cursor.getString(col++));
            site.setMail(cursor.getString(col++));
            site.setPhone(cursor.getString(col++));
            site.setSiteName(cursor.getString(col++));
            site.setSiteId(cursor.getLong(col++));
            long time = cursor.getLong(col++);
            if(time == -1)
                site.setDate(null);
            else
            {
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
                date.setTimeInMillis(time);
                site.setDate(date);
            }
            double lat = cursor.getDouble(col++);
            double lon = cursor.getDouble(col++);
            site.setLoc(new LatLng(lat, lon));
            site.setType(SiteType.values()[cursor.getInt(col)]);
        }
        cursor.close();
        return site;
    }

    public List<BloodSite> getNotifications()
    {
        Cursor cursor = getWritableDatabase().query("notification",null,null,null,null,null,null);
        List<Long> oldNotif = new ArrayList<>();
        List<BloodSite> sites = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        while(cursor.moveToNext())
        {
            int col = 1;
            Long id = cursor.getLong(col++);
            int skip = cursor.getInt(col);
            List<BloodSite> site = getBloodSites(id);
            //for each siteId found check if date is passed and if it was remove item from the notifications
            if(site.size() == 1 && cal.compareTo(site.get(0).getDate()) > 0)
            {
                oldNotif.add(id);
            }
            else
            {
                if(site.size() > 0 && skip == 0)
                    sites.add(site.get(0));
            }
        }

        Comparator<BloodSite> compareDate = new Comparator<BloodSite>() {
            @Override
            public int compare(BloodSite lhs, BloodSite rhs) {
                return lhs.getDate().compareTo(rhs.getDate());
            }
        };

        removeNotifications(oldNotif);
        Collections.sort(sites,compareDate);
        cursor.close();
        return sites;
    }

    private void removeNotifications(List<Long> notificationIds)
    {
        String args[] = new String[1];
        String clause = "siteId = ?";
        for(Long id : notificationIds)
        {
            args[0] = String.valueOf(id);
            getWritableDatabase().delete("notification", clause, args);
        }
    }

    public void scheduleDonation(long siteId, boolean scheduling)
    {
        String whereClause = "id = ? ";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(siteId);
        ContentValues values = new ContentValues();
        if(scheduling)
            values.put("schedule", 1);
        else
            values.put("schedule", 0);


        getWritableDatabase().update("site", values, whereClause, whereArgs);
    }

    public List<BloodSite> getBloodSites(LatLng NE, LatLng SW) {


        List<BloodSite> list = new ArrayList<>();

        String whereClause = "latitude < ? and latitude > ? and longitude < ? and longitude > ? ";
        String[] whereArgs = new String[4];
        whereArgs[0] = String.valueOf(NE.latitude);
        whereArgs[1] = String.valueOf(SW.latitude);
        whereArgs[2] = String.valueOf(NE.longitude);
        whereArgs[3] = String.valueOf(SW.longitude);

        Calendar dateTime = Calendar.getInstance();
        dateTime.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        dateTime.set(Calendar.HOUR_OF_DAY, 0);
        dateTime.set(Calendar.MILLISECOND, 0);
        dateTime.set(Calendar.MINUTE, 0);
        dateTime.set(Calendar.SECOND, 0);
        long dateInMillis = dateTime.getTimeInMillis();


        Cursor cursor = getReadableDatabase().query("site", null, whereClause, whereArgs, null, null, null);
        while(cursor.moveToNext())
        {
            int col = 0;
            long dbId = cursor.getLong(col++);

            int ordinal =cursor.getInt(col++);
            BloodSite site = new BloodSite(Country.values()[ordinal]);
            site.setDbId(dbId);
            site.setAddress(cursor.getString(col++));
            site.setCityName(cursor.getString(col++));
            site.setDetails(cursor.getString(col++));
            site.setMail(cursor.getString(col++));
            site.setPhone(cursor.getString(col++));
            site.setSiteName(cursor.getString(col++));
            site.setSiteId(cursor.getLong(col++));
            long time = cursor.getLong(col++);
            if(time == -1)
                site.setDate(null);
            else
            {
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
                date.setTimeInMillis(time);
                site.setDate(date);
            }
            double lat = cursor.getDouble(col++);
            double lon = cursor.getDouble(col++);
            site.setLoc(new LatLng(lat, lon));
            site.setType(SiteType.values()[cursor.getInt(col)]);
            if(site.getDate() == null || site.getDate().getTimeInMillis() >= dateInMillis)
                list.add(site);
        }
        cursor.close();
        return list;
    }

    public LatLng getUserLastLocation() {
        String[] columns = new String[2];
        columns[0] = "latitude";
        columns[1] = "longitude";
        Cursor cursor = getReadableDatabase().query("user",columns,null,null,null,null,null);
        LatLng loc = new LatLng(0,0);
        if(cursor.moveToFirst())
            loc = new LatLng(cursor.getDouble(0),cursor.getDouble(1));
        cursor.close();
        return loc;
    }

    public Calendar getLastDonationDate() {
        String[] columns = new String[1];
        columns[0] = "date";
        String orderBy = "date DESC";
        Cursor cursor = getReadableDatabase().query("donation", columns, null, null, null, null, orderBy);
        if(cursor.moveToFirst())
        {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(cursor.getLong(0));
            return cal;
        }
        cursor.close();
        return null;
    }

    public List<Long> getAllNotifications()
    {

        List<Long> notifications = new ArrayList<>();

        Cursor cursor = getWritableDatabase().query("notification",null,null,null,null,null,null);
        while(cursor.moveToNext())
        {
            int col = 1;
            Long id = cursor.getLong(col);
            notifications.add(id);
        }
        cursor.close();
        return notifications;
    }

    public void addNotifications(List<Long> newNotifications) {

        List<Long> ids = getAllNotifications();

        for(Long id : newNotifications)
        {
            if(!ids.contains(id)) {
                ContentValues values = new ContentValues();
                values.put("siteId", id);
                values.put("skip", 0);
                getWritableDatabase().insert("notification", null, values);
            }
        }
    }

    public void skipNotifications(List<Long> notifications)
    {
        String whereClause = "siteId = ? ";
        String[] whereArgs = new String[1];
        for(Long id : notifications)
        {
            whereArgs[0] = String.valueOf(id);
            ContentValues values = new ContentValues();
            values.put("skip",1);
            getWritableDatabase().update("notification",values,whereClause,whereArgs);
        }
    }

    public boolean hasShownSupport(){
        String[] column = {"show_support"};
        Cursor cursor = getReadableDatabase().query("user",column,null,null,null,null,null);
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            boolean hasShown = cursor.getInt(0) != 0;
            cursor.close();
            return hasShown;
        }
        return false;
    }

    public void setHasShownSupport(){
        String[] column = {"id"};
        Cursor cursor = getReadableDatabase().query("user",column,null,null,null,null,null);
        cursor.moveToFirst();
        int userId;
        userId = cursor.getInt(0);
        cursor.close();
        String whereClause = "id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(userId);
        ContentValues values = new ContentValues();
        values.put("show_support",true);
        getWritableDatabase().update("user", values,whereClause, whereArgs);
    }

}
