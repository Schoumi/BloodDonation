/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.NewDonationListener;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.SelectionModeListener;
import fr.mobdev.blooddonation.custom_object.DrawerAdapter;
import fr.mobdev.blooddonation.custom_object.DrawerItem;
import fr.mobdev.blooddonation.dialog.AddDonationDialog;
import fr.mobdev.blooddonation.enums.DrawerItemType;
import fr.mobdev.blooddonation.objects.Donation;

public class DonationFragment extends Fragment {

    ListView donation_list;
    List<Donation> donations;
    SelectionModeListener listener;
    boolean isOnSelection;
    List<DrawerItem> items;
    private OnDeletionModeListener onDeletionModeChange;

    public DonationFragment() {
    }

    public static DonationFragment newInstance(OnDeletionModeListener deletionModeListener) {

        DonationFragment fragment = new DonationFragment();
        fragment.setOnDeletionModeChange(deletionModeListener);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.donation, container,false);
        donation_list = v.findViewById(R.id.donation_list);
        items = new ArrayList<>();
        isOnSelection = false;
        listener = new SelectionModeListener() {

            @Override
            public void selectionModeChange(View v) {
                isOnSelection = !isOnSelection;
                onDeletionModeChange.onDeleteModeChange(isOnSelection);
                if(isOnSelection)
                {
                    for(DrawerItem item:items)
                    {
                        item.setType(DrawerItemType.STATE_CHECK);
                    }
                    selectView(v);
                    DrawerAdapter adapter = new DrawerAdapter(getActivity(),R.layout.drawer_item, 0, items);
                    donation_list.setAdapter(adapter);
                }
                else
                {
                    for(DrawerItem item:items)
                    {
                        item.setType(DrawerItemType.STATE_NORMAL);
                        item.setSelected(false);
                    }
                    DrawerAdapter adapter = new DrawerAdapter(getActivity(),R.layout.drawer_item, 0, items);
                    donation_list.setAdapter(adapter);
                }
            }
        };
        donation_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int pos, long id) {
                if(!isOnSelection)
                {
                    listener.selectionModeChange(v);
                }
                else
                {
                    selectView(v);
                }
                return true;
            }
        });
        donation_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int arg2, long id) {
                selectView(v);
            }
        });

        updateDonationList();
        return v;
    }

    private void selectView(View v)
    {
        if(isOnSelection)
        {
            TextView view = v.findViewById(R.id.pos);
            long donationId = Integer.valueOf((String) view.getText());
            for(DrawerItem item:items)
            {
                item.setType(DrawerItemType.STATE_CHECK);
                if(item.getPos() == donationId)
                {
                    item.setSelected(!item.isSelected());
                }
            }
            DrawerAdapter adapter = new DrawerAdapter(getActivity(),R.layout.drawer_item, 0, items);
            donation_list.setAdapter(adapter);
        }
    }

    public void addDonation()
    {
        AddDonationDialog dialog = new AddDonationDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("siteId", -1);
        dialog.setArguments(bundle);
        dialog.setListener(new NewDonationListener() {

            @Override
            public void needUpdate() {
                updateDonationList();
            }
        });
        dialog.show(getFragmentManager(), "add donation dialog");
    }

    public void askRemoveDonation()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_donation_message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        removeSelectedDonation();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void removeSelectedDonation()
    {
        List<Donation> deletedList = new ArrayList<>();
        for(int i = 0; i<items.size(); i++)
        {
            DrawerItem item = items.get(i);
            if(item.isSelected())
            {
                deletedList.add(donations.get(i));
            }
        }
        Database.getInstance(getActivity()).deleteDonation(deletedList);
        listener.selectionModeChange(null);
        updateDonationList();
    }

    private void updateDonationList()
    {
        donations  = Database.getInstance(getActivity()).getDonations();
        items.clear();
        for(Donation don : donations)
        {
            DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
            String texte = df.format(don.getDate().getTime());
            texte += " - ";
            texte += don.getCityName();

            if(don.getCityName().length() > 0)
                texte += " - ";

            switch(don.getDonationType())
            {
                case TOTAL_BLOOD:
                    texte += getResources().getStringArray(R.array.donation_type)[0];
                    break;
                case PLASMA:
                    texte += getResources().getStringArray(R.array.donation_type)[1];
                    break;
                case PLATELET:
                    texte += getResources().getStringArray(R.array.donation_type)[2];
                    break;
                default:
                    //do nothing
                    break;
            }
            DrawerItem item = new DrawerItem(DrawerItemType.STATE_NORMAL, texte);
            items.add(item);
        }
        DrawerAdapter adapter = new DrawerAdapter(getActivity(),R.layout.drawer_item, 0, items);
        donation_list.setAdapter(adapter);
    }

    public void cancelDeletionMode() {
        if(isOnSelection)
            listener.selectionModeChange(null);
    }

    public void setOnDeletionModeChange(OnDeletionModeListener onDeletionModeChange) {
        this.onDeletionModeChange = onDeletionModeChange;
    }

    public boolean isInDeletionMode() {
        return isOnSelection;
    }

    public interface OnDeletionModeListener {
        void onDeleteModeChange(boolean activeDelete);
    }
}
