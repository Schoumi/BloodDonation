/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.car2go.maps.AnyMap;
import com.car2go.maps.OnMapReadyCallback;
import com.car2go.maps.model.CameraPosition;
import com.car2go.maps.model.LatLng;
import com.car2go.maps.model.LatLngBounds;
import com.car2go.maps.model.Marker;
import com.car2go.maps.model.MarkerOptions;
import com.car2go.maps.osm.BitmapDescriptorFactory;
import com.car2go.maps.osm.MapView;
import com.car2go.maps.osm.CameraUpdateFactory;
import com.car2go.maps.osm.MapsConfiguration;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.BuildConfig;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.NetworkListener;
import fr.mobdev.blooddonation.NetworkManager;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.RegisterListener;
import fr.mobdev.blooddonation.dialog.ConfirmDialog;
import fr.mobdev.blooddonation.dialog.InformationDialog;
import fr.mobdev.blooddonation.enums.Country;
import fr.mobdev.blooddonation.enums.SiteType;
import fr.mobdev.blooddonation.objects.BloodSite;
import fr.mobdev.blooddonation.service.AlarmReceiver;
import fr.mobdev.blooddonation.service.BootReceiver;

import static android.content.Context.LOCATION_SERVICE;

public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private AnyMap map;
    private LocationListener locationListener;
    private Geocoder coder;
    private Integer postCode;
    private LatLng previousLocation;
    private float previousZoom;
    private MapView mapView;

    private Country country;
    private Map<Country, LatLng> countryMap;
    private LongSparseArray<Marker> markerList;
    private boolean isFirstLocation;
    private List<BloodSite> siteList;
    private long selectedSite;
    private Marker userCenter;
    private PostCodeUpdateListener postCodeUpdateListener;
    private ImageButton myLocationBt;
    private Location myLocation;


    NetworkListener listener = new NetworkListener() {
        @Override
        public void bloodSiteListChanged(final List<BloodSite> sites) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Database.getInstance(getActivity()).addSitesToDb(sites);
                    AlarmReceiver.prepareNotifications(getActivity());
                    addSitesOnMap();
                }
            });
        }
    };

    public void setPostCodeUpdateListener(PostCodeUpdateListener postCodeUpdateListener) {
        this.postCodeUpdateListener = postCodeUpdateListener;
    }

    private enum CORNER {
        TOP_RIGHT, BOT_LEFT, BOT_RIGHT
    }


    public MapsFragment() {
    }

    public static MapsFragment newInstance(PostCodeUpdateListener postCodeUpdateListener) {
        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setPostCodeUpdateListener(postCodeUpdateListener);
        return mapsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        countryMap = new HashMap<>();
        buildCountryMap();
        country = Country.FRANCE;
        selectedSite = -1;
        userCenter = null;


        if (savedInstanceState != null) {
            isFirstLocation = savedInstanceState.getBoolean("isFirstLocation");
            final double latitude = savedInstanceState.getDouble("locationLatitude");
            final double longitude = savedInstanceState.getDouble("locationLongitude");
            final float zoom = savedInstanceState.getFloat("locationZoom");
            postCode = savedInstanceState.getInt("posteCode");
            previousLocation = new LatLng(latitude,longitude);
            previousZoom = zoom;
            myLocation = savedInstanceState.getParcelable("myLocation");
        } else {
            isFirstLocation = true;
            postCode = -1;
            previousZoom = 14;
            previousLocation = countryMap.get(country);
        }


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(location == null)
                    return;
                myLocation = location;
                myLocationBt.setVisibility(View.VISIBLE);
                if(map != null) {
                    if(userCenter != null)
                        userCenter.remove();

                    MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude()))
                                                    .icon(BitmapDescriptorFactory.getInstance().fromResource(R.drawable.userlocation)).anchor(0.5f,0.5f);
                    userCenter = map.addMarker(markerOptions);
                }
                Database.getInstance(getActivity()).updateUserLoc(location.getLatitude(), location.getLongitude());
                try {
                    List<Address> addrList = coder.getFromLocation(location.getLatitude(), location.getLongitude(), 5);
                    for (Address addr : addrList) {
                        if (addr != null && addr.getPostalCode() != null) {
                            Integer code;
                            try {
                                code = Integer.valueOf(addr.getPostalCode());
                            } catch (NumberFormatException e) {
                                continue;
                            }
                            int original = (code - (code % 1000)) / 1000;
                            if (original == 97) {
                                postCode = (code - (code % 100)) / 100;
                                postCodeUpdateListener.onPostCodeChange(postCode);
                                break;
                            } else if (original > 0 && original < 96) {
                                postCode = (code - (code % 1000)) / 1000;
                                postCodeUpdateListener.onPostCodeChange(postCode);
                                break;
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (isFirstLocation) {
                    map.moveCamera(CameraUpdateFactory.getInstance().newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),14));
                    isFirstLocation = false;
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                //TODO Handle
            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map, container, false);
        final LinearLayout layout = v.findViewById(R.id.info_window);
        layout.setVisibility(View.GONE);
        layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(selectedSite == -1)
                    return;

                InformationDialog dialog = new InformationDialog();
                Bundle args = new Bundle();
                args.putLong("siteId", selectedSite);

                dialog.setArguments(args);
                dialog.show(getFragmentManager(), "information dialog");
                layout.setVisibility(View.GONE);
            }
        });

        myLocationBt = v.findViewById(R.id.move_to_user_location);
        myLocationBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(map != null) {
                    map.animateCamera(CameraUpdateFactory.getInstance().newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),14));
                }
            }
        });
        myLocationBt.setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapsConfiguration.getInstance().initialize(getActivity());
        mapView = view.findViewById(R.id.maps_mv_map);
        mapView.setUserAgent(BuildConfig.APPLICATION_ID);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.getMapAsync(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView = null;
        map = null;
    }

    public void startUpdateLocations() {
        if(getActivity() == null)
            return;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean use_localisation = !prefs.getBoolean("deactivate_localisation", false);
        if(!use_localisation) {
            stopUpdateLocations();
            return;
        }
        locationListener.onLocationChanged(myLocation);

        LocationManager manager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(getActivity(),permissions,1);
        } else {
            if (!prefs.getBoolean("wifi_only", false))
                manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 600000, 50, locationListener);
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 600000, 50, locationListener);
        }

    }

    private void stopUpdateLocations() {
        LocationManager manager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (locationListener != null)
            manager.removeUpdates(locationListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mapView != null)
            mapView.onPause();
        if(map != null)
            stopUpdateLocations();
        myLocationBt.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mapView != null)
            mapView.onResume();
        if(map != null)
            startUpdateLocations();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mapView != null) {
            mapView.onSaveInstanceState(outState);
            outState.putBoolean("isFirstLocation",isFirstLocation);
            outState.putDouble("locationLatitude",previousLocation.latitude);
            outState.putDouble("locationLongitude",previousLocation.longitude);
            outState.putFloat("locationZoom",previousZoom);
            outState.putInt("posteCode",postCode);
            outState.putParcelable("myLocation",myLocation);
        }
        super.onSaveInstanceState(outState);
    }

    public void askWritePermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(getActivity(),permissions,2);
        }
    }

    @Override
    public void onMapReady(AnyMap anyMap) {
        map = anyMap;
        userCenter = null;
        askWritePermission();
        map.animateCamera(CameraUpdateFactory.getInstance().newLatLngZoom(previousLocation,previousZoom));
        startUpdateLocations();

        country = Country.FRANCE;
        countryMap = new HashMap<>();
        markerList = new LongSparseArray<>();
        buildCountryMap();

        map.setOnCameraChangeListener(new AnyMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                previousZoom = cameraPosition.zoom;
                previousLocation = cameraPosition.target;
                loadData();
            }
        });
        map.setOnMarkerClickListener(new AnyMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker == userCenter)
                    return false;
                View v = getView();
                assert v != null;
                LinearLayout layout = v.findViewById(R.id.info_window);
                layout.setVisibility(View.VISIBLE);


                int idx = markerList.indexOfValue(marker);
                if(idx < 0 || idx >= markerList.size())
                    return false;
                long id = markerList.keyAt(idx);
                selectedSite = id;
                BloodSite infoSite = null;
                for(BloodSite site : siteList) {
                    if(site.getDbId() == id) {
                        infoSite = site;
                        break;
                    }
                }

                if(infoSite == null)
                    return false;

                TextView tv = v.findViewById(R.id.info_title);
                tv.setText(infoSite.getCityName());
                tv = v.findViewById(R.id.info_snippet);
                if (infoSite.getDate() != null) {
                    DateFormat df = DateFormat.getDateInstance(DateFormat.FULL,	Locale.getDefault());
                    df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
                    tv.setText(df.format(infoSite.getDate().getTime()));
                    tv.setVisibility(View.VISIBLE);
                } else {
                    tv.setVisibility(View.GONE);
                }
                return true;
            }
        });
        map.setOnMapClickListener(new AnyMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                View v = getView();
                assert v != null;
                LinearLayout layout = v.findViewById(R.id.info_window);
                layout.setVisibility(View.GONE);
                selectedSite = -1;
            }
        });

        initPosition();
        BootReceiver.startNextChecking(getActivity(), false);
    }

    private LatLng getCorner(CORNER corner) {
        if(map == null)
            return new LatLng(0,0);
        com.car2go.maps.Projection pr = map.getProjection();
        LatLngBounds bounds = pr.getVisibleRegion().latLngBounds;
        if(corner == CORNER.TOP_RIGHT)
            return bounds.northeast;
        else if(corner == CORNER.BOT_LEFT)
            return bounds.southwest;
        else if (corner == CORNER.BOT_RIGHT)
            return new LatLng(bounds.southwest.latitude,bounds.northeast.longitude);
        else
            return new LatLng(bounds.northeast.latitude,bounds.southwest.longitude);
    }

    private void checkPastDonation() {
        BloodSite site = Database.getInstance(getActivity()).getPastUnregisteredDonation();
        if (site != null) {
            ConfirmDialog dialog = new ConfirmDialog();
            Bundle args = new Bundle();
            args.putLong("siteId", site.getDbId());
            dialog.setArguments(args);
            dialog.setRegisterListener(new RegisterListener() {

                @Override
                public void processNewRegistration() {
                    checkPastDonation();
                }
            });
            dialog.show(getFragmentManager(),"ConfirmationDialogInformation");
        }
    }

    private void loadData() {

        LatLng NECorner = getCorner(CORNER.TOP_RIGHT);
        LatLng SWCorner = getCorner(CORNER.BOT_LEFT);

        NetworkManager.getInstance(listener, getActivity()).loadFromNet(country, NECorner, SWCorner);
        addSitesOnMap();
    }


    private void addSitesOnMap() {

        LatLng top_right = getCorner(CORNER.TOP_RIGHT);
        LatLng bot_left = getCorner(CORNER.BOT_LEFT);

        siteList = Database.getInstance(getActivity()).getBloodSites(top_right,bot_left);
        // clean marker that will not be anymore on maps
        List<Long> idList = new ArrayList<>();
        for (BloodSite site : siteList) {
            idList.add(site.getDbId());
        }

        List<Long> idToRemove = new ArrayList<>();
        for (int i = 0; i < markerList.size(); i++) {
            if (!idList.contains(markerList.keyAt(i))) {
                idToRemove.add(markerList.keyAt(i));
            }
        }
        for (Long id : idToRemove) {
            Marker marker = markerList.get(id);
            marker.remove();
            markerList.delete(id);
        }

        for (BloodSite site : siteList) {
            if (markerList.indexOfKey(site.getDbId()) < 0) {
                MarkerOptions options = new MarkerOptions();
                options.position(new LatLng(site.getLoc().latitude, site.getLoc().longitude));

                int res;
                if (site.getCountry() == Country.FRANCE) {
                    if (site.getType().equals(SiteType.efs))
                        res = R.drawable.efs;
                    else if (site.getType().equals(SiteType.mobile_blood))
                        res = R.drawable.blood;
                    else if (site.getType().equals(SiteType.mobile_plasma))
                        res = R.drawable.plasma;
                    else
                        res = R.drawable.mixed;
                    options.icon(BitmapDescriptorFactory.getInstance().fromResource(res));
                    options.anchor(0.5f,0.5f);
                }
                markerList.put(site.getDbId(),map.addMarker(options));
            }
        }
    }

    private void buildCountryMap() {
        countryMap.put(Country.FRANCE, new LatLng(47.084444, 2.396389));
    }

    private void initPosition() {
        //
        LatLng loc = Database.getInstance(getActivity()).getUserLastLocation();

        if(!loc.equals(new LatLng(0,0)))
        {
            map.moveCamera(CameraUpdateFactory.getInstance().newLatLngZoom(new LatLng(loc.latitude,loc.longitude),14));
        }
        else
        {
            map.moveCamera(CameraUpdateFactory.getInstance().newLatLngZoom(previousLocation,previousZoom));
        }
        coder = new Geocoder(getActivity());
        loadData();
        checkPastDonation();
    }

    public interface PostCodeUpdateListener {
        void onPostCodeChange(int postcode);
    }

}
