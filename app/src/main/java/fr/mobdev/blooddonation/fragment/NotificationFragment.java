/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;
import fr.mobdev.blooddonation.dialog.InformationDialog;
import fr.mobdev.blooddonation.objects.BloodSite;

public class NotificationFragment extends Fragment {

    private List<Long> notificationsIds;
    private List<Long> readedNotifications;

    public NotificationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.notifications,container,false);
        notificationsIds = new ArrayList<>();
        readedNotifications = new ArrayList<>();
        buildListView(v);
        ListView view = v.findViewById(R.id.notifications_list);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if(position != notificationsIds.size()) {
                        long siteId = notificationsIds.get(position);
                        Bundle args = new Bundle();
                        args.putLong("siteId", siteId);
                        InformationDialog dialog = new InformationDialog();
                        dialog.setArguments(args);
                        dialog.show(getFragmentManager(), "information dialog");
                        notificationsIds.remove(id);
                        readedNotifications.add(siteId);
                    }
                    else
                    {
                        readedNotifications.addAll(notificationsIds);
                        Database.getInstance(getActivity()).skipNotifications(readedNotifications);
                        buildListView(v);
                    }
                }
        });
        return v;
    }

    @Override
    public void onDestroy()
    {
        Database.getInstance(getActivity()).skipNotifications(readedNotifications);
        super.onDestroy();
    }



    private void buildListView(View v)
    {
        if(v == null)
            return;
        notificationsIds.clear();
        ListView view = v.findViewById(R.id.notifications_list);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),R.layout.simple_list_item);
        Comparator<BloodSite> compareDate = new Comparator<BloodSite>() {
            @Override
            public int compare(BloodSite lhs, BloodSite rhs) {
                return lhs.getDate().compareTo(rhs.getDate());
            }
        };

        List<BloodSite> notifications = Database.getInstance(getActivity()).getNotifications();
        Collections.sort(notifications, compareDate);

        for(BloodSite site : notifications)
        {

            DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
            String date = df.format(site.getDate().getTime());
            String notif = date + " - " + site.getCityName();
            adapter.add(notif);

            notificationsIds.add(site.getDbId());
        }
        if(notifications.size() > 1)
            adapter.add(getString(R.string.clear));
        else
            adapter.add(getString(R.string.no_notif));
        view.setAdapter(adapter);
    }

}
