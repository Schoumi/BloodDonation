/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

public class PrefFragment extends PreferenceFragmentCompat implements OnSharedPreferenceChangeListener {


	private OnEditPersonnalInfoListener onEditPersonnalInfoListener;

	public static PrefFragment newInstance(OnEditPersonnalInfoListener personnalInfoListener) {
		PrefFragment fragment = new PrefFragment();
		fragment.setOnEditPersonnalInfoListener(personnalInfoListener);
		return fragment;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		Preference pref = findPreference("info_change");
		
		pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				onEditPersonnalInfoListener.onEditPesonnalInfoClick();
				return true;
			}
		});
		
		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,String key) {
		if(key.equals("deactivate_localisation"))
		{
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					//update user location to the location in his profile
					HashMap<String,Object> userData = Database.getInstance(getActivity()).getUserInformation();
					Geocoder coder = new Geocoder(getActivity());
					String city = (String) userData.get("CityName");
					String postal = (String) userData.get("Postal");
					try {
						List<Address> addr = coder.getFromLocationName(city+" "+postal, 1);
						if(!addr.isEmpty())
						{
							Database.getInstance(getActivity()).updateUserLoc(addr.get(0).getLatitude(), addr.get(0).getLongitude());
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();
			
		}
		
	}

	public void setOnEditPersonnalInfoListener(OnEditPersonnalInfoListener onEditPersonnalInfoListener) {
		this.onEditPersonnalInfoListener = onEditPersonnalInfoListener;
	}

	public interface OnEditPersonnalInfoListener {
		void onEditPesonnalInfoClick();
	}
	
}
