/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;

public class DonationInfoFragment extends Fragment {


    public DonationInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.donation_info,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View v = getView();
        assert v != null;
        HashMap<String, Object> maps = Database.getInstance(getActivity()).getUserInformation();

        String userName = (String) maps.get("UserName");
        String blood = (String) maps.get("BloodGroup");
        String city = (String) maps.get("CityName");
        int D = (Integer) maps.get("D");
        int C = (Integer) maps.get("C");
        int E = (Integer) maps.get("E");
        int c = (Integer) maps.get("c");
        int e = (Integer) maps.get("e");
        int K = (Integer) maps.get("K");

        TextView view = v.findViewById(R.id.user_name);
        view.setText(userName);
        view = v.findViewById(R.id.blood_group);
        view.setText(blood);
        view = v.findViewById(R.id.city_name);
        view.setText(city);
        view = v.findViewById(R.id.rhesus);
        String rhesus = "D" + getStringFor(D) +" C" + getStringFor(C) +" E"+ getStringFor(E) +" c"+ getStringFor(c) +" e" + getStringFor(e) + " K"+getStringFor(K);
        view.setText(rhesus);
    }

    private String getStringFor(int plus)
    {
        if(plus == 0)
            return "-";
        else
            return "+";
    }
}
