/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.Database;
import fr.mobdev.blooddonation.R;

public class InformationFragment extends Fragment {

    private boolean modify;
    private InformationListener informationListener;


    public InformationFragment() {
    }

    public static InformationFragment newInstance(boolean modify, InformationListener listener) {
        InformationFragment fragment = new InformationFragment();
        fragment.modify = modify;
        fragment.informationListener = listener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.init_view,container,false);
        Button bt = view.findViewById(R.id.validate_button);

        CheckedTextView dView = view.findViewById(R.id.blood_view_D);
        CheckedTextView majCView =  view.findViewById(R.id.blood_view_C);
        CheckedTextView majEView =  view.findViewById(R.id.blood_view_E);
        CheckedTextView minCView =  view.findViewById(R.id.blood_view_c);
        CheckedTextView minEView =  view.findViewById(R.id.blood_view_e);
        CheckedTextView kView = view.findViewById(R.id.blood_view_K);

        final View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckedTextView view = (CheckedTextView) v;
                view.setChecked(!view.isChecked());

            }
        };

        if(modify)
        {
            HashMap<String, Object> map = Database.getInstance(container.getContext()).getUserInformation();

            Spinner blood_view =  view.findViewById(R.id.blood_spinner);
            List<String> group_array = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.blood_array)));
            String bloodGroup = (String)map.get("BloodGroup");
            blood_view.setSelection(group_array.indexOf(bloodGroup));

            Spinner gender_view =  view.findViewById(R.id.gender_spinner);

            EditText name_view = view.findViewById(R.id.name_text);
            name_view.setText((String)map.get("UserName"));

            EditText city_name_view =  view.findViewById(R.id.city_text);
            city_name_view.setText((String)map.get("CityName"));

            EditText postal_code_view = view.findViewById(R.id.postal_text);
            postal_code_view.setText((String)map.get("Postal"));

            dView.setChecked((Integer)map.get("D") != 0);
            majCView.setChecked((Integer)map.get("C") != 0);
            majEView.setChecked((Integer)map.get("E") != 0);
            minCView.setChecked((Integer)map.get("c") != 0);
            minEView.setChecked((Integer)map.get("e") != 0);
            kView.setChecked((Integer)map.get("K") != 0);
            int gender = 0;
            if (map.get("Gender").equals(0))
                gender = 1;

            gender_view.setSelection(gender);
        }

        dView.setOnClickListener(listener);
        majCView.setOnClickListener(listener);
        majEView.setOnClickListener(listener);
        minCView.setOnClickListener(listener);
        minEView.setOnClickListener(listener);
        kView.setOnClickListener(listener);

        bt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Spinner blood_view = view.findViewById(R.id.blood_spinner);
                Spinner gender_view = view.findViewById(R.id.gender_spinner);
                EditText name_view =  view.findViewById(R.id.name_text);
                EditText city_name_view =  view.findViewById(R.id.city_text);
                EditText postal_code_view =  view.findViewById(R.id.postal_text);
                CheckedTextView dView =  view.findViewById(R.id.blood_view_D);
                CheckedTextView majCView =  view.findViewById(R.id.blood_view_C);
                CheckedTextView majEView =  view.findViewById(R.id.blood_view_E);
                CheckedTextView minCView =  view.findViewById(R.id.blood_view_c);
                CheckedTextView minEView =  view.findViewById(R.id.blood_view_e);
                CheckedTextView kView = view.findViewById(R.id.blood_view_K);

                String blood = blood_view.getSelectedItem().toString();
                String gender = gender_view.getSelectedItem().toString();
                boolean isMale;
                isMale = gender.equals(getResources().getStringArray(R.array.gender_array)[0]);

                String name = name_view.getText().toString();
                String city = city_name_view.getText().toString();
                String postal = postal_code_view.getText().toString();

                Database.getInstance(container.getContext()).updateUserInfo(
                        name, isMale, city, postal, blood, dView.isChecked(),
                        majCView.isChecked(), majEView.isChecked(),
                        minCView.isChecked(), minEView.isChecked(),
                        kView.isChecked());
                informationListener.onValidate(!modify);
            }
        });
        return view;
    }

    public interface InformationListener {
        void onValidate(boolean isFirstTime);
    }

}
