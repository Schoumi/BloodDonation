/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import fr.mobdev.blooddonation.R;

public class AboutFragment extends Fragment {

    private OnLicenceClickListener licenceClickListener;

    public static AboutFragment newInstance(OnLicenceClickListener onLicenceClickListener) {
        AboutFragment fragment = new AboutFragment();
        fragment.setOnLicenceClickListener(onLicenceClickListener);
        return fragment;
    }

    private void setOnLicenceClickListener(OnLicenceClickListener onLicenceClickListener) {
        licenceClickListener = onLicenceClickListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.about, container, false);
        ImageView liberapay = v.findViewById(R.id.donate);
        liberapay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://fr.liberapay.com/Schoumi";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        ImageView tipeee = v.findViewById(R.id.donate_tipeee);
        tipeee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://tipeee.com/schoumi";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        TextView tv = v.findViewById(R.id.licence);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                licenceClickListener.onLicenceClick();
            }
        });

        return v;
    }

    public interface OnLicenceClickListener {
        void onLicenceClick();
    }
}
