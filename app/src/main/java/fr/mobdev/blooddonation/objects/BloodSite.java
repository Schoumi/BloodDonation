/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.objects;

import java.util.Calendar;

import android.location.Location;

import com.car2go.maps.model.LatLng;

import fr.mobdev.blooddonation.enums.Country;
import fr.mobdev.blooddonation.enums.SiteType;

public class BloodSite {
	private Country country;
	private long dbId; //id in application database
	private long siteId; //obtain with c_id
	private SiteType type;//obtain by parsing icon (verify that there's no solution unknown and that give the real answer)
	private LatLng loc;//obtain with lat and lon
	private Calendar date;//obtain with c_date
	private String siteName; //obtain with lp_libconv
	private String address;//obtain by parsing text
	private String details;//obtain by parsing text
	private String cityName;//obtain with ville
	private String phone;//obtain with num_tel
	private String mail;//obtain with adr_mail
	private boolean hasBeenScheduled;//if user plan's to go to it by adding it in agenda
	
	//private float zoomAppearance;//may be delete or use later don't know

	public BloodSite(Country ctr) {
		country = ctr;
		siteId = -1;
		dbId = -1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((loc == null) ? 0 : loc.hashCode());
		result = prime * result
				+ ((siteName == null) ? 0 : siteName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}	

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setLoc(LatLng latLng) {
		loc = latLng;
	}

	public void setType(SiteType type) {
		this.type = type;
	}

	public LatLng getLoc()
	{
		return loc;
	}

	public Country getCountry()
	{
		return country;
	}

	public SiteType getType() {
		return type;
	}

	public long getDbId() {
		return dbId;
	}

	public void setDbId(long dbId) {
		this.dbId = dbId;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public boolean isHasBeenScheduled() {
		return hasBeenScheduled;
	}

	public void setHasBeenScheduled(boolean hasBeenScheduled) {
		this.hasBeenScheduled = hasBeenScheduled;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		BloodSite other = (BloodSite) obj;
		
		if (country != other.country)
			return false;
		
		if (date == null) 
		{
			if (other.date != null)
				return false;
		} 
		else if (!date.equals(other.date))
		{
			return false;
		}
		
		Location location = new Location("custom");
		Location otherLocation = new Location("custom");
		if(loc != null)
		{
			location.setLatitude(loc.latitude);
			location.setLongitude(loc.longitude);
		}
		
		if(other.loc != null)
		{
				otherLocation.setLatitude(other.loc.latitude);
				otherLocation.setLongitude(other.loc.longitude);
		}
		
		if (loc == null) 
		{
			if (other.loc != null)
				return false;
		}
		else if(other.loc == null)
		{
			return false;
		}
		else if (location.distanceTo(otherLocation) > 300)
		{
			return false;
		}
		
		if (type != other.type)
			return false;
		
		return true;
	}
}
