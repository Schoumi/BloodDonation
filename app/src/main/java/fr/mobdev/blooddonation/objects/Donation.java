/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.blooddonation.objects;

import java.util.Calendar;

import fr.mobdev.blooddonation.enums.DonationType;

public class Donation {
	private DonationType type;
	private Calendar date;
	private long siteId;
	private long donationId;
	private String city_name;
	
	public Donation(DonationType type,Calendar date, long siteId, long donationId, String city_name){
		this.date = date;
		this.siteId = siteId;
		this.type = type;
		this.donationId = donationId;
		this.city_name = city_name;
	}
	
	public long getSiteId(){
		return siteId;
	}
	
	public Calendar getDate(){
		return date;
	}
	
	public DonationType getDonationType(){
		return type;
	}
	
	public long getDonationId(){
		return donationId;
	}

	public String getCityName() {
		return city_name;
	}

	public void setCityName(String city_name) {
		this.city_name = city_name;
	}
}
